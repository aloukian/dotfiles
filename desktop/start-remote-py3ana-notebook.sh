HOST=psanaphi103
PORT=24551
RAYPORT=24141
SSHPORT=2222

ssh -t -L$PORT:localhost:$PORT -L$RAYPORT:localhost:$RAYPORT -L$SSHPORT:$HOST:22 -o ExitOnForwardFailure=yes -Y \
  pslogin "ssh -t -L$PORT:localhost:$PORT -L$RAYPORT:localhost:$RAYPORT -o ExitOnForwardFailure=yes -Y \
    $HOST 'source ~/.bash_profile; source enable-analysis-environment.sh; \
    jupyter notebook --no-browser --port $PORT'"
