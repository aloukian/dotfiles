HOST=psanagpu105
PORT=24551
RAYPORT=24141
CONDA_SOURCE=/reg/g/psdm/etc/psconda.sh
ssh -t -L$PORT:localhost:$PORT -L$RAYPORT:localhost:$RAYPORT -o ExitOnForwardFailure=yes -Y \
  pslogin "ssh -t -L$PORT:localhost:$PORT -L$RAYPORT:localhost:$RAYPORT -o ExitOnForwardFailure=yes -Y \
    $HOST 'source $CONDA_SOURCE; conda activate ana-1.3.53; \
    jupyter notebook --no-browser --port $PORT'"
