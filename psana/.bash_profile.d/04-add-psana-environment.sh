# add production conda to psana
. /reg/g/psdm/sw/conda/inst/miniconda2-prod-rhel7/etc/profile.d/conda.sh
export LD_LIBRARY_PATH=/afs/slac.stanford.edu/package/lsf/curr/lib
export SIT_DATA=/reg/g/psdm/data
export SIT_ROOT=/reg/g/psdm
